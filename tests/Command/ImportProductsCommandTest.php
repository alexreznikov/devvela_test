<?php

declare(strict_types=1);

namespace App\Tests\Command;

use App\Entity\Product;
use App\Repository\ProductRepository;
use RuntimeException;
use SimpleXMLElement;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Tester\CommandTester;

class ImportProductsCommandTest extends KernelTestCase
{
    private CommandTester $commandTester;

    private const PRODUCTS_DATA = [
        [
            'product_id' => 1,
            'title' => 'Product 1',
            'description' => 'Product 1 description',
            'rating' => '5',
            'inet_price' => 123,
            'price' => 123,
            'image' => 'image1.jpg',
        ],
        [
            'product_id' => 2,
            'title' => 'Product 2',
            'description' => 'Product 2 description',
            'rating' => '4.5',
            'price' => 456,
            'image' => 'image2.jpg',
        ]
    ];

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $command = $application->find('app:import-products');
        $this->commandTester = new CommandTester($command);
    }

    public function testWrongUrlExecute()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The url should be valid url.');

        $this->commandTester->execute([
            'url' => 'wrong_url',
        ]);
    }

    public function testExecute()
    {
        $tmpXmlFile = $this->getTmpXmlFile($this->generateXml());

        $this->commandTester->execute([
            'url' => 'file://' . $tmpXmlFile,
        ]);

        $count = count(self::PRODUCTS_DATA);

        /** @var ProductRepository $repository */
        $repository = $this->getContainer()->get(ProductRepository::class);

        for ($key = 0; $key < $count; $key++) {
            $this->assertEquals(
                $this->getProductEntity($key),
                $repository->findAllPaginated($key + 1, 1)->getResults()[0]
            );
        }

        $this->commandTester->assertCommandIsSuccessful();

        $this->assertStringContainsString(
            "$count products was successfully imported",
            $this->commandTester->getDisplay()
        );

        $this->commandTester->execute([
            'url' => 'file://' . $tmpXmlFile,
        ]);

        $this->assertStringContainsString("$count products was skipped", $this->commandTester->getDisplay());

        unlink($tmpXmlFile);
    }

    private function getTmpXmlFile(string $xml): string
    {
        $tmpXmlFile = tempnam(sys_get_temp_dir(), 'products_xml');

        $fp = fopen($tmpXmlFile, 'w');
        fwrite($fp, $xml);
        fclose($fp);

        return $tmpXmlFile;
    }

    private function generateXml(): string
    {
        $xml = new SimpleXMLElement('<products/>');
        foreach (self::PRODUCTS_DATA as $productData) {
            $product = $xml->addChild('product');

            foreach ($productData as $attribute => $value) {
                $product->addChild($attribute, (string)$value);
            }
        }

        return $xml->asXML();
    }

    private function getProductEntity(int $key): Product
    {
        $productData = self::PRODUCTS_DATA[$key] ?? null;

        if ($productData === null) {
            throw new RuntimeException('Unknown product test data.');
        }

        $product = new Product();
        $product->setProductId($productData['product_id'])
            ->setTitle($productData['title'])
            ->setDescription($productData['description'])
            ->setRating($productData['rating'])
            ->setPrice($productData['price'])
            ->setImage($productData['image']);

        if (array_key_exists('inet_price', $productData)) {
            $product->setInetPrice($productData['inet_price']);
        }

        return $product;
    }
}
