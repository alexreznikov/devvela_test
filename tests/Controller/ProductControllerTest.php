<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Entity\Product;
use App\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductControllerTest extends WebTestCase
{
    private const PRODUCTS_DATA = [
        [
            'product_id' => 1,
            'title' => 'Product 1',
            'description' => 'Product 1 description',
            'rating' => '5',
            'inet_price' => 123,
            'price' => 123,
            'image' => 'image1.jpg',
        ],
        [
            'product_id' => 2,
            'title' => 'Product 2',
            'description' => 'Product 2 description',
            'rating' => '4.5',
            'price' => 456,
            'image' => 'image2.jpg',
        ]
    ];

    public function testList(): void
    {
        $client = static::createClient();
        $this->loadListFixtures($client);

        $client->request('GET', '/api/products');

        $this->assertResponseIsSuccessful();

        $this->assertJsonStringEqualsJsonString(
            json_encode([
                'products' => self::PRODUCTS_DATA,
                'pagination' => [
                    'page' => 1,
                    'page_size' => Paginator::PAGE_SIZE,
                    'total' => count(self::PRODUCTS_DATA),
                ],
            ]),
            $client->getResponse()->getContent()
        );
    }

    private function loadListFixtures(KernelBrowser $client): void
    {
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();

        foreach (self::PRODUCTS_DATA as $productData) {
            $product = new Product();
            $product->setProductId($productData['product_id'])
                ->setTitle($productData['title'])
                ->setDescription($productData['description'])
                ->setRating($productData['rating'])
                ->setPrice($productData['price'])
                ->setImage($productData['image']);

            if (array_key_exists('inet_price', $productData)) {
                $product->setInetPrice($productData['inet_price']);
            }

            $entityManager->persist($product);
        }

        $entityManager->flush();
    }
}
