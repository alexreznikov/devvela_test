Тестовое задание
========================

Требования
------------

  * PHP 8.1.0 или выше;
  * PDO-SQLite PHP расширение должно быть включено;
  * [Symfony CLI][1];
  * и [обычные требования к приложению Symfony][2].

Установка
------------

```bash
$ git clone git@gitlab.com:alexreznikov/devvela_test.git
$ cd devvela_test
$ symfony composer install
$ php bin/console doctrine:database:create
$ php bin/console doctrine:migrations:migrate
$ php bin/console --env=test doctrine:database:create
$ php bin/console --env=test doctrine:schema:create
```

Запуск веб сервера
-----

```bash
$ symfony serve --port=3000
```

Запуск импорта
-----

```bash
$ php bin/console app:import-products http://127.0.0.1:3000/products.xml
```

Документация
-----

http://127.0.0.1:3000/api/doc

Тесты
-----

```bash
$ ./bin/phpunit
```

[1]: https://symfony.com/download#step-1-install-symfony-cli
[2]: https://symfony.com/doc/current/setup.html#technical-requirements
