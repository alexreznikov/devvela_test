<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Product;
use App\Normalizer\ApiGroups;
use App\Repository\ProductRepository;
use Doctrine\Common\Annotations\AnnotationReader;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ProductController extends AbstractController
{
    /**
     * @throws ExceptionInterface
     */
    #[Route('/api/products', name: 'products_list', methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: 'Returns the list of products',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'products',
                    type: 'array',
                    items: new OA\Items(ref: new Model(type: Product::class)),
                    example: [
                        'title' => 'Product 1',
                        'description' => 'Product 1 description',
                        'rating' => '4.5',
                        'price' => 123,
                        'image' => 'image1.jpg',
                        'product_id' => 123,
                        'inet_price' => 123,
                    ],
                ),
                new OA\Property(
                    property: 'pagination',
                    properties: [
                        new OA\Property(
                            property: 'page',
                            type: 'integer',
                            minimum: 1,
                            example: 1,
                        ),
                        new OA\Property(
                            property: 'page_size',
                            type: 'integer',
                            example: '10',
                        ),
                        new OA\Property(
                            property: 'total',
                            type: 'integer',
                            example: 100,
                        ),
                    ],
                    type: 'object',
                ),
            ],
            type: 'object',
        ),
    )]
    #[OA\Parameter(
        name: 'page',
        in: 'query',
        schema: new OA\Schema(type: 'integer'),
    )]
    public function list(Request $request, ProductRepository $productRepository): JsonResponse
    {
        $page = (int)$request->get('page', 1);

        $productsPaginator = $productRepository->findAllPaginated($page);

        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $normalizer = new ObjectNormalizer($classMetadataFactory);

        return $this->json([
            'products' => array_map(
                static fn(Product $product) => $normalizer->normalize(
                    $product,
                    null,
                    [
                        AbstractNormalizer::GROUPS => [ApiGroups::PRODUCTS_LIST],
                        AbstractObjectNormalizer::SKIP_NULL_VALUES => true,
                    ]
                ),
                iterator_to_array($productsPaginator->getResults())
            ),
            'pagination' => [
                'page' => $productsPaginator->getCurrentPage(),
                'page_size' => $productsPaginator->getPageSize(),
                'total' => $productsPaginator->getNumResults(),
            ],
        ]);
    }
}
