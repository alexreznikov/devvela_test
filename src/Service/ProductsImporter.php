<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Product;
use App\Service\ProductProviders\ProviderInterface;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Throwable;

final class ProductsImporter
{
    private int $imported = 0;
    private int $skipped = 0;
    private int $failed = 0;

    public function __construct(
        private readonly ProviderInterface $fetchProducts,
        private readonly ManagerRegistry $doctrine,
        private readonly LoggerInterface $logger,
    ) {
    }

    private function ensure(): void
    {
        $this->imported = 0;
        $this->skipped = 0;
        $this->failed = 0;
    }

    public function import(): void
    {
        $this->ensure();

        $entityManager = $this->doctrine->getManager();

        $imported = 0;
        $skipped = 0;

        /** @var Product[] $productsBatch */
        foreach ($this->fetchProducts->batch() as $productsBatch) {
            $productIds = [];
            foreach ($productsBatch as $product) {
                $productIds[] = $product->getProductId();
            }

            $existsIdsMap = array_flip(
                $this->doctrine->getRepository(Product::class)->findExistsIds($productIds)
            );

            foreach ($productsBatch as $product) {
                if (array_key_exists($product->getProductId(), $existsIdsMap)) {
                    $skipped++;
                    continue;
                }

                $entityManager->persist($product);
                $imported++;
            }

            try {
                $entityManager->flush();
                $entityManager->clear();

                $this->imported = $imported;
                $this->skipped = $skipped;
            } catch (Throwable $e) {
                $this->doctrine->resetManager();
                $this->failed += count($productsBatch);

                $this->logger->error($e);
            }
        }
    }

    public function getImported(): int
    {
        return $this->imported;
    }

    public function getSkipped(): int
    {
        return $this->skipped;
    }

    public function getFailed(): int
    {
        return $this->failed;
    }
}
