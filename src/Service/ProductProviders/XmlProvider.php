<?php

declare(strict_types=1);

namespace App\Service\ProductProviders;

use App\Entity\Product;
use Generator;
use InvalidArgumentException;
use SimpleXMLElement;
use XMLReader;

class XmlProvider implements ProviderInterface
{
    private const TAG_PRODUCT = 'product';

    private ?string $source = null;

    public function setSource(string $source): void
    {
        $this->source = $source;
    }

    public function each(): Generator
    {
        if ($this->source === null) {
            throw new InvalidArgumentException('Source must be set.');
        }

        $xml = new XMLReader();
        $xml->open($this->source);

        while ($xml->read()) {
            if ($xml->nodeType !== XMLReader::ELEMENT || $xml->name !== self::TAG_PRODUCT) {
                continue;
            }

            $productXml = new SimpleXMLElement($xml->readOuterXml());

            $productId = (int)$productXml->product_id;
            $title = trim((string)$productXml->title);
            $description = trim((string)$productXml->description);
            $rating = (string)$productXml->rating;
            $price = (int)$productXml->price;
            $inetPrice = isset($productXml->inet_price) && is_numeric((string)$productXml->inet_price)
                ? (int)$productXml->inet_price
                : null;
            $image = (string)$productXml->image;

            $product = (new Product())
                ->setProductId($productId)
                ->setTitle($title)
                ->setDescription($description)
                ->setRating($rating)
                ->setPrice($price)
                ->setInetPrice($inetPrice)
                ->setImage($image);

            yield $product;
        }

        $xml->close();
    }

    public function batch(int $size = self::DEFAULT_BATCH_SIZE): Generator
    {
        $batch = [];
        foreach ($this->each() as $product) {
            $batch[] = $product;

            if (count($batch) === $size) {
                yield $batch;
                $batch = [];
            }
        }

        if (count($batch) > 0) {
            yield $batch;
        }
    }
}
