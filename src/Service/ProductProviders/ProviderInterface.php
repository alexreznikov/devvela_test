<?php

declare(strict_types=1);

namespace App\Service\ProductProviders;

use App\Entity\Product;
use Generator;

interface ProviderInterface
{
    const DEFAULT_BATCH_SIZE = 100;

    public function setSource(string $source): void;

    /**
     * @return Generator<Product>
     */
    public function each(): Generator;

    /**
     * @param int $size
     * @return Generator<Product>
     */
    public function batch(int $size = self::DEFAULT_BATCH_SIZE): Generator;
}
