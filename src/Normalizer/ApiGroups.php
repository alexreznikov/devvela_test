<?php

declare(strict_types=1);

namespace App\Normalizer;

abstract class ApiGroups
{
    public const PRODUCTS_LIST = 'api/products';
}
