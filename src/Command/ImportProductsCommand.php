<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\ProductProviders\ProviderInterface;
use App\Service\ProductsImporter;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:import-products',
    description: 'Import products from remote XML',
)]
final class ImportProductsCommand extends Command
{
    use LockableTrait;

    private SymfonyStyle $io;

    public function __construct(
        private readonly ManagerRegistry $managerRegistry,
        private readonly LoggerInterface $logger,
        private readonly ProviderInterface $provider,
        string $name = null
    ) {
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this->addArgument('url', InputArgument::REQUIRED, 'Products XML url');
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return Command::SUCCESS;
        }

        $url = $input->getArgument('url');

        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            $this->release();
            throw new InvalidArgumentException('The url should be valid url.');
        }

        $this->provider->setSource($url);
        $productsImporter = new ProductsImporter($this->provider, $this->managerRegistry, $this->logger);

        $productsImporter->import();

        $imported = $productsImporter->getImported();
        $skipped = $productsImporter->getSkipped();
        $failed = $productsImporter->getFailed();

        if ($imported > 0) {
            $this->io->success(sprintf('%d products was successfully imported', $imported));
        }

        if ($skipped > 0) {
            $this->io->note(sprintf('%d products was skipped', $skipped));
        }

        if ($failed > 0) {
            $this->io->error(sprintf('%d products was failed to import', $failed));
        }

        $this->release();

        if ($failed > 0) {
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}
