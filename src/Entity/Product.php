<?php

namespace App\Entity;

use App\Normalizer\ApiGroups;
use App\Repository\ProductRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    #[ORM\Column]
    #[Groups([ApiGroups::PRODUCTS_LIST])]
    private ?int $product_id = null;

    #[ORM\Column(length: 255)]
    #[Groups([ApiGroups::PRODUCTS_LIST])]
    private ?string $title = null;

    #[ORM\Column(length: 5000)]
    #[Groups([ApiGroups::PRODUCTS_LIST])]
    private ?string $description = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 2, scale: 1)]
    #[Groups([ApiGroups::PRODUCTS_LIST])]
    private ?string $rating = null;

    #[ORM\Column(options: ['unsigned' => true])]
    #[Groups([ApiGroups::PRODUCTS_LIST])]
    private ?int $price = null;

    #[ORM\Column(nullable: true, options: ['unsigned' => true])]
    #[Groups([ApiGroups::PRODUCTS_LIST])]
    private ?int $inet_price = null;

    #[ORM\Column(length: 255)]
    #[Groups([ApiGroups::PRODUCTS_LIST])]
    private ?string $image = null;

    public function getProductId(): ?int
    {
        return $this->product_id;
    }

    public function setProductId(int $product_id): self
    {
        $this->product_id = $product_id;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getRating(): ?string
    {
        return $this->rating;
    }

    public function setRating(string $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getInetPrice(): ?int
    {
        return $this->inet_price;
    }

    public function setInetPrice(?int $inet_price): self
    {
        $this->inet_price = $inet_price;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }
}
